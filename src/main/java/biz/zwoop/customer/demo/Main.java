package biz.zwoop.customer.demo;

import biz.zwoop.customer.demo.components.ConfigurableDemo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;

/**
 * Created by jimmy.
 */
@SpringBootApplication
@EnableSpringConfigured
public class Main implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        new ConfigurableDemo().run();
    }
}
