package biz.zwoop.customer.demo.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * Created by jimmy.
 */
@Configurable
public class ConfigurableDemo {

    @Autowired
    private PrintLineService printLineService;

    public void run() {
        printLineService.printLine();
    }
}
