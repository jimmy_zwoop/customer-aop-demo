package biz.zwoop.customer.demo.components;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by jimmy.
 */
@Component
public class PrintLineService {

    private Logger logger = LoggerFactory.getLogger(PrintLineService.class);

    public void printLine() {
        logger.info("Hello World");
    }
}
